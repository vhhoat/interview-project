export const REQUEST_USERS = 'REQUEST_USERS';
export const RECEIVE_USERS = 'RECEIVE_USERS';

export const REQUEST_SEARCH = 'REQUEST_SEARCH';
export const RECEIVE_SEARCH = 'RECEIVE_SEARCH';

export const requestUsers = () => ({
  type: REQUEST_USERS
});

export const receiveUsers = (json) => ({
  type: RECEIVE_USERS,
  users: json.express
});

export const requestSearch = (value) => ({
  type: REQUEST_SEARCH,
  keyword: value
});

export const receiveSearch = (json) => ({
  type: RECEIVE_SEARCH,
  results: json.express
});

export const fetchSearch = (keyword) => dispatch => {
  dispatch(requestSearch(keyword));
  return fetch('/api/search?q='+ keyword)
    .then(response => response.json())
    .then(json => dispatch(receiveSearch(json)))
}

export const fetchUsers = () => dispatch => {
  dispatch(requestUsers());
  return fetch('/api/hello')
    .then(response => response.json())
    .then(json => dispatch(receiveUsers(json)))
}