Thank you for applying to Ringblaze.

## Usage

Install [nodemon](https://github.com/remy/nodemon) globally

```
npm i nodemon -g
```

Install server and client dependencies

```
yarn
cd server
yarn
cd ../client
yarn
```

To start the server and client at the same time

```
yarn dev
```

## Tasks
1. Add an endpoint to return all the users by reading the data in `data` folder
2. Using Twitter Bootstrap & replace the code that printing 'Hello from Express' on the homepage with a table showing all the users by fetching the data from the endpoint you added. Don't spend too much time on the UI though. We just need a functional page.

Make sure to commit all your changes using git. Install any additional packages you want such as enzyme, etc. if needed. Good luck!
